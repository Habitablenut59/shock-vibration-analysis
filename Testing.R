library(ggplot2)
library(plotly)
library(MESS)
library(signal)
library(baseline)
library(tidyverse)

read_plus <- function(flnm) {
  read_csv(flnm) %>% 
    mutate(filename = flnm)
}

dataset <-list.files(pattern = "*.CSV", full.names = T) %>% map_df(~read_plus(.))
i = 1
ListofPossibilities <- read.csv('ListofPossibilites.CSV', header = FALSE)
comb <- ListofPossibilities[i,]
subsetdata <- subset(dataset, grepl(comb,dataset$filename))
list <- data.frame(unique(subsetdata$filename))
nr <-nrow(list)

r = 1
reqs <- paste('Test', r, sep = "" )
comb1 <- list [r,]
spedata <- subset(subsetdata, grepl(comb1, subsetdata$filename))
assign(reqs, spedata)


attach(totalenergy)
def <- manova(cbind(Freq, Maximumacc, Clegg) ~ SurfaceSys * Distance)
def
summary(def)
summary.aov(def)
t <- aov(Clegg ~ SurfaceSys + Distance)
TukeyHSD(t, 'ses' )



maybe <- manova(cbind(`Maximum Frequency`,Maximumacc, MaximumFreqAcc,`Median Freq`,Energy)~ Surface*SubsurfaceDistance)
maybe
summary(maybe)
summary.aov(maybe)
########################################################################################################################

plot(Test1$Seconds, Test1$Magnitude, type = 'l')
sec <- Test1$Seconds
velx <- cumtrapz(Test1$X,Test1$Seconds)
velx <- c(velx)
vely <- cumtrapz(Test1$Y,Test1$Seconds)
vely <- c(vely)
velz <- cumtrapz(Test1$Z,Test1$Seconds)
velz <- c(velz)
vel <- sqrt(velx^2 + vely^2 + velz^2)
plot(sec, vel, type = 'l')
disx <- cumtrapz(velx,sec)
disx <- c(disx)
disy <- cumtrapz(vely,sec)
disy <- c(disy)
disz <- cumtrapz(velz,sec)
disz <- c(disz)
dis <- sqrt(disx^2 + disy^2 + disz^2)
#dis <- cumtrapz(vel,sec)

ggplot(FrequencyVibration, aes(y = F2, x = SS1)) +
  geom_col() +
  xlab('Surface System')+
  ylab('Median Frequency') +
  ggtitle('Test Tite', subtitle = 'Test') +
  theme_classic()
