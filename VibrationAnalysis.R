library(ggplot2)
library(plotly)
library(MESS)
library(tidyverse)
library(baseline)
library(signal)
#Input Variables

sr <- 313 #Sampling Rate
sn <- 256 #Sample Number

#######################################Functions##################################################
read_plus <- function(flnm) {
  read_csv(flnm) %>%
    mutate(filename = flnm)
}
responsegraph <-
  function(xvar, yvar, graphtitle) {
    #Graphs Shock Repsonse Spectrum
    dat <- data.frame(xvar, yvar)
    OverallAmpl <-
      ggplot(dat, aes(xvar, yvar)) + 
      geom_line() + theme_gray() + 
      xlab('Frequency') +
      ylab('Acceleration') + 
      ggtitle(paste('Shock Response Spectrum from', graphtitle)) + 
      xlim(0, tail(Frequency,  n = 1)) + ylim(0, 2.5)
    ggplotly(OverallAmpl)
  }
magnitudegraph <-
  function(xvar, yvar, graphtitle) {
    #Graphs Magnitude Over Time
    dat <- data.frame(xvar, yvar)
    OverallAmpl <-
      ggplot(dat, aes(xvar, yvar)) +
      geom_line() + theme_gray() + 
      xlab('Time') +
      ylab('Acceleration') +
      ggtitle(paste('Magnitude over time from', graphtitle)) #+ xlim(0,tail(Frequency,  n = 1)) + ylim(0,2.5)
    ggplotly(OverallAmpl)
  }

averagedataframe <- function(column1, column2, column3) {
  datAmp <- data.frame(column1, column2, column3)
  rowMeans(datAmp)
}
Graphsave <- function(prefix = NULL, savename, savefigure) {
  #Save Shock Graph
  savefile <- paste0(prefix, savename , '.html')
  htmlwidgets::saveWidget(as_widget(savefigure), savefile)
}

surfacename <-
  function(filetomine) {
    #Creates surface name from Filename
    Surface <-
      setClass(
        'Surface',
        slots = list(
          titlename = 'character',
          distance = 'numeric',
          surfacename = 'character',
          subname = 'character' ,
          replication = 'numeric',
          trial = 'character'
        )
      )
    name <-
      data.frame(strsplit(filetomine, '/',  fixed = TRUE)) #Splits the ./ from useful bit
    name <-
      data.frame(strsplit(name[2, ], '.',  fixed = TRUE)) #Splits the periods
    surfacename <- name[1, ] #Isolates Surface
    subsurfacename <- name[2, ] #Isolates Subsurface
    distancename <- name[3, ] #Isolates Distance
    repname <- name[4, ] #Isolates Rep Number
    trialname <- name[5, ] #Isolates Trial Number
    primarysurfacename <-
      subset(Naming.Conventions, Short == surfacename) #Generates Surface Name
    subname <-
      subset(Naming.Conventions, Short == subsurfacename) #Generates Subsurface name
    distname <-
      subset(Distance.Conventions, Short == distancename) #Generates Distance Name
    replname <-
      subset(Naming.Conventions, Short == repname) #Generates Replication name
    if (nchar(subsurfacename) == 0) {
      surfname <-
        paste(replname$Long,
              primarysurfacename$Long,
              'from',
              distname$Long) #Surfaces without subsurface
      #print('Short')
      Surface(
        titlename = surfname ,
        distance = distname$Shortish ,
        surfacename = primarysurfacename$Long ,
        subname = 'None',
        replication = replname$Shortish ,
        trial = trialname
      )
    } else {
      surfname <-
        paste(
          replname$Long,
          primarysurfacename$Long,
          "on",
          subname$Long,
          'from',
          distname$Long
        ) #Surfaces with specific subsurface
      #print('Long')
      Surface(
        titlename = surfname ,
        distance = distname$Shortish ,
        surfacename = primarysurfacename$Long ,
        subname = subname$Long ,
        replication = replname$Shortish ,
        trial = trialname
      )
    }
    
    
  }

magnitude <- function(xvar, yvar, zvar) {
  sqrt(xvar ^ 2 + yvar ^ 2 + zvar ^ 2)
}

FFTMagnitude <- function(xvar, yvar, zvar, SampleNumber) {
  #Filter X-axis
  FFTmagx <-
    (abs(fft(xvar)) * 2 / SampleNumber) #Absolute value of the divided by half the sampling rate
  Magnx <- FFTmagx[1:(SampleNumber / 2)]
  #plot(Frequency,Magnx, type = 'l')
  
  #Filter Y-axis
  FFTmagy <- (abs(fft(yvar)) * 2 / SampleNumber)
  Magny <- FFTmagy[1:(SampleNumber / 2)]
  #plot(Frequency,Magny, type = 'l')
  
  #Filter Z-axis
  FFTmagz <- (abs(fft(zvar)) * 2 / SampleNumber)
  Magnz <- FFTmagz[1:(SampleNumber / 2)]
  #plot(Frequency,Magnz, type = 'l')
  
  sqrt(Magnx ^ 2 + Magny ^ 2 + Magnz ^ 2)
}

SecondstoFrequency <- function(SamplingRate, SamplingNumber) {
  n <- (0:SamplingNumber)
  FFTFreq <- (n * (SamplingRate / SamplingNumber))
  FFTFreq[1:(SamplingNumber / 2)]
}

summarydata <- function(dataframetobind, xvar, yvar, zvar, surface) {
  maximum <- max(zvar)
  dat.sum <- data.frame(xvar, yvar)
  dat.sum <- dat.sum[1:127, ]
  medfreq <-
    dat.sum$xvar[which(dat.sum$yvar == median(dat.sum$yvar))]
  dfadd <-
    data.frame(
      paste0(surface@surfacename, surface@subname),
      surface@surfacename,
      surface@subname,
      surface@distance,
      surface@replication,
      surface@trial,
      maximum,
      medfreq
    )
  names(dfadd) <-
    c(
      'SurfaceSys',
      'Surface',
      'Subsurface',
      'Distance',
      'Replication',
      'Trial',
      'Maximumacc',
      'Median Freq'
    )
  rbind(dataframetobind, dfadd)
}

baselinecorrect <- function(dataframe) {
  bf <- butter(8, .04, type = 'high')
  transposed <- t(dataframe)
  mode(transposed) <- 'numeric'
  X <- baseline(transposed['X', , drop = FALSE])
  Y <- baseline(transposed['Y', , drop = FALSE])
  Z <- baseline(transposed['Z', , drop = FALSE])
  X <- t(X@corrected)
  Y <- t(Y@corrected)
  Z <- t(Z@corrected)
  X <- c(X)
  Y <- c(Y)
  Z <- c(Z)
  X <- filtfilt(bf, X)
  Y <- filtfilt(bf, Y)
  Z <- filtfilt(bf, Z)
  dataframe$X <- X
  dataframe$Y <- Y
  dataframe$Z <- Z
  return(dataframe)
}

####################################Proper Script######################################################


{
  setwd("~/Projects/shock-vibration-analysis")
  Distance.Conventions <-
    read.csv('Distance.CSV') #loads distance convention
  Naming.Conventions <-
    read.csv('Naming Conventions.CSV') #loads naming convention
  setwd("~/Projects/shock-vibration-analysis/Data")
  dataset <-
    list.files(pattern = "*.CSV", full.names = T) %>% map_df( ~ read_plus(.))
  setwd('..')
  totalenergy <- data.frame()
  Listed <- data.frame(unique(dataset$filename))
  nr <- nrow(Listed)
  for (i in 1:nr) {
    print(i)
    comb <- Listed[i, ]
    subsetdata <- subset(dataset, grepl(comb, dataset$filename))
    subsetdata <- baselinecorrect(subsetdata)
    Amplitude <- FFTMagnitude(subsetdata$X, subsetdata$Y, subsetdata$Z, sn)
    Magnitude <- magnitude(subsetdata$X, subsetdata$Y, subsetdata$Z)
    setwd('~/Projects/shock-vibration-analysis/.InteractiveCharts')
    Frequency <- SecondstoFrequency(sr, sn)
    surface <- surfacename(comb)
    #print(surface)
    # AverageSeconds <-
    #   averagedataframe(Test1$Seconds, Test2$Seconds, Test3$Seconds)
    # figure <-
    #   responsegraph(Frequency, Amplitude, surface@titlename)
    # #Graphsave(prefix = 'Frequency', surface, figure)
    # figure1 <-
    #   magnitudegraph(AverageSeconds, Magnitude, surface@titlename)
    #Graphsave(prefix = 'Magnitude', surface, figure1)
    totalenergy <-
      summarydata(totalenergy,
                  Frequency,
                  Amplitude,
                  Magnitude,
                  surface)
  }
  write.csv(
    totalenergy,
    "~/Projects/shock-vibration-analysis/.InteractiveCharts/Output.CSV",
    row.names = FALSE
  )
  print('Done Bitches')
}
